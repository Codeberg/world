# codeberg world

Workadventu.re page for Codeberg.

The map can be played at

<https://play.workadventu.re/_/global/raw.codeberg.page/Codeberg/world/@pages/space.json>

## Optimizing

Unfortunately the maps should be optimized prior to deployment.

The steps are included in the Makefile.

The Makefile target "docker" creates a docker image including the necessary nodejs-based software.

The Makefile target "build" checks out the pages branch (on which tha map resides) and starts the optimizer. This creates a pages directory which includes a checkout of the pages branch.

The Makefile target "push" commits the changed files in the pages directory and pushes it to the pages branch.

The Makefile target "clean" removes the pages directory.

The pages directory is included in the .gitignore file.

## Tile-Sets

The cert-tilesets are from <https://github.com/c3CERT/rc3_tiles.git>.
See LICENSE.md for individual licenses.

## Changes

The map can be changed with the [Tiled Editor](https://www.mapeditor.org/).

## Further Reading

- <https://howto.rc3.world/2021/maps.html>
- <https://workadventu.re/map-building/>
