FROM node:12.22.12-stretch-slim
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y git
RUN mkdir /opt/optimizer
RUN chown node: /opt/optimizer
USER node
RUN git clone https://github.com/thecodingmachine/map-optimizer /opt/optimizer
RUN cd /opt/optimizer && yarn upgrade
