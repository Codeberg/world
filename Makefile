.PHONY: all clean build push docker
all: docker build

docker:
	docker build . -t map-optimizer-latest

clean:
	rm -rf pages

pages:
	git clone $$(git remote get-url origin) -b pages pages
	cp -R images pages

build: pages
	docker run -v ${PWD}:/home/node/input map-optimizer /opt/optimizer/bin/tile-optimizer -i /home/node/input/main.json -o /home/node/input/pages
	docker run -v ${PWD}:/home/node/input map-optimizer /opt/optimizer/bin/tile-optimizer -i /home/node/input/space.json -o /home/node/input/pages

push: build
	cd pages && git add * && git commit -m "new deployment by Makefile" && git push
